package com.home;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {

        InputStream inputStream = new FileInputStream("/Users/user/IdeaProjects/Collections/src/com/home/Import_User_Sample_en.csv");
        int length = inputStream.available();
        byte[] data = new byte[length];
        inputStream.read(data);

        String text = new String(data);

        ArrayList<String[]> lineWords = new ArrayList<>();
        String[] lines = text.split("\n");

        for (String line : lines) {
//            System.out.println(line);
//            System.out.println("- - - - - - - - - - - - - - - - - - - - -");
            String[] words = line.split(",");
            lineWords.add(words);
        }

        for (String[] words : lineWords) { //вывод информации согласно условию
            if (words[6].equals("123451")) {
                for (String word : words) {
                    System.out.println(word);
                    System.out.print("|");
                }
                System.out.println("_____________________________________");
            }
        }

//        for (String[] words : lineWords) {
//
//        }

    }
}
